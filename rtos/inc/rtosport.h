/*
 * Function prototypes that are handled by the different ports but need to be defined in rtos functions
 */
#ifndef RTOSPORT_H_
#define RTOSPORT_H_

int printf(const char* format, ...);

#endif /* RTOSPORT_H_ */
