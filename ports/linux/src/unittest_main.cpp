#include <CppUTest/CommandLineTestRunner.h>

int main(int argc, const char* argv[]) {
	return CommandLineTestRunner::RunAllTests(argc, argv);
}
// Need to import test groups since we are linking in the test lib
IMPORT_TEST_GROUP(TimeServiceTest);
