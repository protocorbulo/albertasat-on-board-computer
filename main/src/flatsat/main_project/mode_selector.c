#include "mode_selector.h"

enum {
	INIT_MODE,
	INITIAL_STANDBY,
	POWER_SAFE_MODE,
	DETUMBLE_MODE,
	SCIENCE_MODE
};

static int mode;

void mode_selector( void *pvParameters )
{
/* Declares */

	/* As per most tasks, this task is implemented in an infinite loop. */
	for( ;; )
	{
		/* task runtime */

		if ( VBATT < VSAFE ) mode = POWER_SAFE_MODE;

	}
}
/*-----------------------------------------------------------*/