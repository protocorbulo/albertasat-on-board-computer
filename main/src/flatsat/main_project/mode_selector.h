/*
 * modeselector.h
 *
 *  Created on: Oct 3, 2014
 *      Author: sdamkjar
 */

#ifndef MODE_SELECTOR_H_
#define MODE_SELECTOR_H_

void mode_selector( void *pvParameters );

#endif /* MODE_SELECTOR_H_ */