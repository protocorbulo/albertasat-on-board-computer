

/* FreeRTOS.org includes. */
#include "FreeRTOS.h"
#include "task.h"

/* LPC1769 interface includes. */
#include "basic_io.h"

/* Additional includes. */
#include "defines.h"

/*-----------------------------------------------------------*/
/* Task/Thread Prototypes. */
#include "mode_selector.h"

int main( void )
{
	/* Init the semi-hosting. */
	printf( "\n" );

	/* Create tasks/threads. */
	xTaskCreate( mode_selector, "Mode Selector", 240, NULL, 1, NULL );

	/* Start the scheduler so tasks start executing. */
	vTaskStartScheduler();

	/* If all is well we will never reach here as the scheduler will now be
	running.  If we do reach here then it is likely that there was insufficient
	heap available for the idle task to be created. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
	/* This function will only be called if an API call to create a task, queue
	or semaphore fails because there is too little heap RAM remaining. */
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName )
{
	/* This function will only be called if a task overflows its stack.  Note
	that stack overflow checking does slow down the context switch
	implementation. */
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
	/* This example does not use the idle hook to perform any processing. */
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
	/* This example does not use the tick hook to perform any processing. */
}


