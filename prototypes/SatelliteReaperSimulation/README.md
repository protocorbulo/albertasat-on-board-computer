#Satelltie Reaper Simulation

Why? Because our satellite is so awsome it goes around with its three cubes
and steels cubes from the 49 other cube sats. They weren't prepared for use.

The src folder was renamed to Application.

##Simulation Purpose

This simulation implements the firmware architecture using software mockups of hardware devices.

##Environment

The project was made in Eclipse 3.8 under Lubuntu.

##How to get involved

To get involved with work on this simulation email Brendan - bbruner@ualberta.ca.