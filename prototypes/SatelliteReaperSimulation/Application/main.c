
#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#define MAIN_PRIO ( tskIDLE_PRIORITY + 1)

static void mainTask(void *);

int main(void)
{
	xTaskCreate(mainTask,
				"main task",
				20,
				NULL,
				MAIN_PRIO,
				NULL);
	vTaskStartScheduler();
	return 0;
}

static void mainTask(void *p)
{
	char const *msg = "main task\n";

	for(;;)
	{
		printf("%s", msg);
		vTaskDelay(1000 / portTICK_RATE_MS);
	}
}

