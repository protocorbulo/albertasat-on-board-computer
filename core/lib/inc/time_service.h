/*
 * time_service.h
 */
/*
#ifndef TIME_SERVICE_H_
#define TIME_SERVICE_H_
*/
#pragma once
#include <stdint.h>

void TimeService_setTimestamp(uint32_t time);
uint32_t TimeService_timestamp();
void TimeService_reset();
uint32_t TimeService_recordFirstBootTime();
uint32_t TimeService_firstBootTime();
uint32_t TimeService_secondsAfterFirstBoot();
//#endif /* TIME_SERVICE_H_ */
