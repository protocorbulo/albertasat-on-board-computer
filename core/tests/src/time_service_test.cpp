extern "C" {
#include "time_service.h"
}
#include <CppUTest/TestHarness.h>

TEST_GROUP(TimeServiceTest){
	void setup(){
		TimeService_reset();
	}
};

//Should be able to set / get the timestamp
TEST(TimeServiceTest, SetGet){
    uint32_t expected = 12345678;
    TimeService_setTimestamp(expected);
    uint32_t actual = TimeService_timestamp();
    CHECK_EQUAL(expected, actual);
}
// Should be able to reset the timestamp using reset
TEST(TimeServiceTest, reset_timestamp){
	TimeService_setTimestamp(12345);
    TimeService_reset();
    CHECK_EQUAL(0, TimeService_timestamp());
}

// Should be able to record to first boot time
TEST(TimeServiceTest, recordFirstBootTime){
	uint32_t expected = 123456;
	TimeService_setTimestamp(expected);
	uint32_t actual1 = TimeService_recordFirstBootTime();
	uint32_t actual2 = TimeService_firstBootTime();
	CHECK_EQUAL(expected, actual1);
	CHECK_EQUAL(expected, actual2);
}

// If recording first boot time before timestamp set, should return 0
TEST(TimeServiceTest, recordFirstBootTime_early){
    uint32_t actual = TimeService_recordFirstBootTime();
    CHECK_EQUAL(0, actual);
}

//Should be able to reset the first boot time using reset
TEST(TimeServiceTest, reset_firstBootTime){
	TimeService_setTimestamp(123456);
	TimeService_recordFirstBootTime();
	TimeService_reset();
	uint32_t actual = TimeService_firstBootTime();
	CHECK_EQUAL(0, actual);
}
//Should return 0 (indicating an error) if try to record the first boot time twice without a reset
TEST(TimeServiceTest, recordFirstBootTime_twice_no_reset){
	uint32_t expected = 485716;
    TimeService_setTimestamp(expected);
    TimeService_recordFirstBootTime();
    uint32_t actual = TimeService_recordFirstBootTime();
    CHECK_EQUAL(0,actual);
    actual = TimeService_firstBootTime();
    CHECK_EQUAL(expected, actual);
}
// Should re-record first boot if a reset in between calls
TEST(TimeServiceTest, recordFirstBootTime_twice_with_reset){
	uint32_t expected = 7665543;
	TimeService_setTimestamp(expected - 1000);
	TimeService_recordFirstBootTime();
	TimeService_reset();
	TimeService_setTimestamp(expected);
	uint32_t actual = TimeService_recordFirstBootTime();
	CHECK_EQUAL(expected, actual);
	actual = TimeService_firstBootTime();
	CHECK_EQUAL(expected, actual);
}
// Should get the time since boot
TEST(TimeServiceTest, secondsAfterFirstBoot){
	uint32_t initialTime = 123596;
	uint32_t expected = 1250;
    TimeService_setTimestamp(initialTime);
    TimeService_recordFirstBootTime();
    TimeService_setTimestamp(initialTime + expected);
    uint32_t actual = TimeService_secondsAfterFirstBoot();
    CHECK_EQUAL(expected, actual);
}
//Should return 0 (meaning error) if trying to get time after first boot before it was set
TEST(TimeServiceTest, secondsAfterFirstBoot_not_set){
    TimeService_setTimestamp(3333);
    uint32_t actual = TimeService_secondsAfterFirstBoot();
    CHECK_EQUAL(0, actual);
}
// Should return 0 (meaning error) if timestamp < firstBootTime
TEST(TimeServiceTest, secondsAfterFirstBoot_timestamp_lt_firstBootTime){
    TimeService_setTimestamp(9000);
    TimeService_recordFirstBootTime();
    TimeService_setTimestamp(8000);
    uint32_t actual = TimeService_secondsAfterFirstBoot();
    CHECK_EQUAL(0, actual);
}
